package com.example.orderservice.controller;

import com.alibaba.fastjson.JSONObject;
import com.example.basedomains.dto.Order;
import com.example.basedomains.dto.OrderEvent;
import com.example.orderservice.service.OrderProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/orders")
public class OrdersController {



    @Autowired
    private OrderProducer orderProducer;

    @RequestMapping(path = "/send", method = RequestMethod.POST)
    public String send(@RequestBody Order dto) {

        OrderEvent orderEvent = new OrderEvent();
        Order order = new Order();
        order.setOrderId(UUID.randomUUID().toString());
        order.setName(dto.getName());
        orderEvent.setStatus("PENDING");
        orderEvent.setMessage("Order status on pending state");
        orderEvent.setOrder(order);
        try{
            orderProducer.sendMessage(JSONObject.toJSONString(orderEvent));
            return "Sukses Send Produce";
        }catch (Exception e){
            return "Failed Send Produce because "+ e.getLocalizedMessage();
        }

    }

}
